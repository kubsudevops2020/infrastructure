import json
from datetime import datetime
import time
import pandas as pd
from matplotlib import pyplot

from pyzabbix import ZabbixAPI, ZabbixMetric, ZabbixSender

def detect(json_hist, what_methods=(1, 1, 1, 1, 1, 1, 1, 1, 1)):

	data = pd.read_json(json.dumps(json_hist))
	data['clock'] = pd.to_datetime(data['clock'], unit='s')
	data.index=data["clock"]


	###
	### PART 1
	###
	df = data["value"]
	df = df.asfreq(freq='3min', fill_value=0)

	from adtk.data import validate_series
	df = validate_series(df)
	print(df)

	from adtk.visualization import plot

	plot(df)

	###
	### PART 2
	###

	anomaly = False

	# 2.1 ThresholdAD
	if (what_methods[0]):
		from adtk.detector import ThresholdAD
		threshold_ad = ThresholdAD(high=20000, low=0)
		anomalies = threshold_ad.detect(df)
		ax = plot(df, anomaly=anomalies, ts_linewidth=1, ts_markersize=3,
				  anomaly_markersize=5, anomaly_color='red', anomaly_tag="marker")
		ax[0].set_title('ThresholdAD')
		if (anomalies[anomalies==1].size >= 0):
			anomaly = True

	# 2.2 QuantileAD
	if (what_methods[1]):
		from adtk.detector import QuantileAD
		quantile_ad = QuantileAD(high=0.91, low=0)
		anomalies = quantile_ad.fit_detect(df)
		ax = plot(df, anomaly=anomalies, ts_linewidth=1, ts_markersize=3,
				  anomaly_markersize=5, anomaly_color='red', anomaly_tag="marker")
		ax[0].set_title('QuantileAD')
		if (anomalies[anomalies==1].size >= 0):
			anomaly = True

	# 2.3 InterQuartileRangeAD
	if (what_methods[2]):
		from adtk.detector import InterQuartileRangeAD
		iqr_ad = InterQuartileRangeAD(c=1.5)
		anomalies = iqr_ad.fit_detect(df)
		ax = plot(df, anomaly=anomalies, ts_linewidth=1, ts_markersize=3,
				  anomaly_markersize=5, anomaly_color='red', anomaly_tag="marker")
		ax[0].set_title('InterQuartileRangeAD')
		if (anomalies[anomalies==1].size >= 0):
			anomaly = True

	# 2.4 GeneralizedESDTestAD
	if (what_methods[3]):
		from adtk.detector import GeneralizedESDTestAD
		esd_ad = GeneralizedESDTestAD(alpha=0.3)
		anomalies = esd_ad.fit_detect(df)
		ax = plot(df, anomaly=anomalies, ts_linewidth=1, ts_markersize=3,
				  anomaly_markersize=5, anomaly_color='red', anomaly_tag="marker")
		ax[0].set_title('GeneralizedESDTestAD')
		if (anomalies[anomalies==1].size >= 0):
			anomaly = True

	# 2.5 PersistAD
	if (what_methods[4]):
		from adtk.detector import PersistAD
		persist_ad = PersistAD(c=11.0, side='positive')
		anomalies = persist_ad.fit_detect(df)
		ax = plot(df, anomaly=anomalies, ts_linewidth=1, ts_markersize=3, anomaly_color='red')
		ax[0].set_title('PersistAD')
		if (anomalies[anomalies==1].size >= 0):
			anomaly = True

	# 2.6 LevelShiftAD
	if (what_methods[5]):
		from adtk.detector import LevelShiftAD
		level_shift_ad = LevelShiftAD(c=6.0, side='both', window=5)
		anomalies = level_shift_ad.fit_detect(df)
		ax = plot(df, anomaly=anomalies, anomaly_color='red')
		ax[0].set_title('LevelShiftAD')
		if (anomalies[anomalies==1].size >= 0):
			anomaly = True

	# 2.7 AutoregressionAD
	if (what_methods[6]):
		from adtk.detector import AutoregressionAD
		autoregression_ad = AutoregressionAD(n_steps=30, step_size=1, c=3.0)
		anomalies = autoregression_ad.fit_detect(df)
		ax = plot(df, anomaly=anomalies, ts_markersize=1, anomaly_color='red', anomaly_tag="marker",
				  anomaly_markersize=2)
		ax[0].set_title('AutoregressionAD')
		if (anomalies[anomalies==1].size >= 0):
			anomaly = True

	# 2.8 MinClusterDetector
	if (what_methods[7]):
		from adtk.detector import MinClusterDetector
		from sklearn.cluster import KMeans
		min_cluster_detector = MinClusterDetector(KMeans(n_clusters=3))
		anomalies = min_cluster_detector.fit_detect(pd.DataFrame(data=df))
		ax = plot(df, anomaly=anomalies, ts_linewidth=1, ts_markersize=3, anomaly_color='red',
				  anomaly_alpha=0.3, curve_group='all')
		ax[0].set_title('MinClusterDetector')
		if (anomalies[anomalies==1].size >= 0):
			anomaly = True

	# 2.9 SeasonalAD
	if (what_methods[8]):
		from adtk.detector import SeasonalAD
		seasonal_ad = SeasonalAD()
		anomalies = seasonal_ad.fit_detect(df)
		ax = plot(df, anomaly=anomalies, ts_markersize=1, anomaly_color='red',
				  anomaly_tag="marker", anomaly_markersize=2)
		ax[0].set_title('SeasonalAD')
		if (anomalies[anomalies==1].size >= 0):
			anomaly = True

	pyplot.show()
	return anomaly




zapi = ZabbixAPI(url='http://194.177.**.***/', user='Admin', password='*****')

# Создать временной отрезок
time_till = int(time.mktime(datetime.now().timetuple()))
time_from = time_till - 60 * 60 * 24  # 24 hours

# Получить историю

hist = zapi.history.get(history = 3, hostids=10319,	itemids=30691,
						time_from=time_from, time_till=time_till)

print(len(hist))
has_anomaly = detect(hist)


metrics= [ZabbixMetric('172.18.0.1', 'system.cpu.num', str(int(has_anomaly)), int(time.mktime(datetime.now().timetuple())))]
sender = ZabbixSender(zabbix_server='194.177.22.203', zabbix_port=10051)
print(metrics)
print(sender)
print(sender.send(metrics))